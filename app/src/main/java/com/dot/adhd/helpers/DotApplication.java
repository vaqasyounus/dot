package com.dot.adhd.helpers;

import android.app.Application;


/**
 * Created by Muhammad Waqas on 18/06/2017.
 *
 */
public class DotApplication extends Application {


    @Override
    public void onCreate() {
        super.onCreate();
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/to-the-point.regular.ttf");
    }
}
