package com.dot.adhd.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;

import com.dot.adhd.models.MonitorDataModel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;

import lecho.lib.hellocharts.model.PointValue;

/**
 * Created by Muhammad Waqas on 3/8/2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "DOT_DB";
    private static final int DATABASE_VERSION = 1;

    private static DatabaseHelper dInstance = null;
    private static Context mContext;


    public static DatabaseHelper getInstance(Context context) {

        if (dInstance == null) {
            dInstance = new DatabaseHelper(context.getApplicationContext());
        }
        mContext = context;
        return dInstance;
    }


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public final String TABLE_MONITOR_DATA = "MonitorData";
    public final String KEY_ID = "id";
    public final String KEY_THETA = "theta";
    public final String KEY_BETA = "beta";
    public final String KEY_ALPHA = "alpha";
    public final String KEY_DELTA = "delta";
    public final String KEY_THETA_BETA_RATIO = "thetaBetaRatio";
    public final String KEY_THETA_ALPHA_RATIO = "thetaAlphaRatio";
    public final String KEY_EEG = "eeg";
    public final String KEY_TIME_STAMP = "timeStamp";
    public final String KEY_SYNC_STATUS = "syncStatus";

    String CREATE_TABLE_MONITOR_DATA = "CREATE TABLE " + TABLE_MONITOR_DATA + " ("
            + KEY_ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,"
            + KEY_THETA + " DOUBLE,"
            + KEY_BETA + " DOUBLE,"
            + KEY_ALPHA + " DOUBLE,"
            + KEY_DELTA + " DOUBLE,"
            + KEY_THETA_BETA_RATIO + " DOUBLE,"
            + KEY_THETA_ALPHA_RATIO + " DOUBLE,"
            + KEY_EEG + " DOUBLE,"
            + KEY_TIME_STAMP + " INTEGER,"
            + KEY_SYNC_STATUS + " INTEGER DEFAULT 0"
            + " )";

    public long addMonitorData(MonitorDataModel monitorDataModel) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();

            values.put(KEY_THETA, monitorDataModel.getTheta());
            values.put(KEY_ALPHA, monitorDataModel.getAlpha());
            values.put(KEY_DELTA, monitorDataModel.getDelta());
            values.put(KEY_BETA, monitorDataModel.getBeta());
            values.put(KEY_THETA_BETA_RATIO, monitorDataModel.getTheta_beta_ratio());
            values.put(KEY_THETA_ALPHA_RATIO, monitorDataModel.getTheta_alpha_ratio());
            values.put(KEY_EEG, monitorDataModel.getEeg());
            values.put(KEY_TIME_STAMP, monitorDataModel.getCreatedAt());
            values.put(KEY_SYNC_STATUS, monitorDataModel.getSyncStatus());

            long i = db.insert(TABLE_MONITOR_DATA, null, values);
            return i;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        } finally {
            db.close();
            exportDatabse();
        }
    }

    public long updateMonitorData(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_SYNC_STATUS, 1);
            long i = db.update(TABLE_MONITOR_DATA, values, KEY_ID + " = " + id, null);
            return i;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        } finally {
            db.close();
            exportDatabse();
        }
    }

    public long updateMonitorData2(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_SYNC_STATUS, 0);
            long i = db.update(TABLE_MONITOR_DATA, values, KEY_ID + " = " + id, null);
            return i;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        } finally {
            db.close();
            exportDatabse();
        }
    }

    public void deleteAllData(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+ TABLE_MONITOR_DATA);
    }

    public ArrayList<MonitorDataModel> getAllMonitoredData() {
        ArrayList<MonitorDataModel> monitorDataModels = new ArrayList<MonitorDataModel>();
        Cursor cursor = null;
        try {
            String sqlquery = "SELECT * FROM " + TABLE_MONITOR_DATA;

            SQLiteDatabase db = this.getWritableDatabase();
            cursor = db.rawQuery(sqlquery, null);

            if (cursor.moveToFirst()) {
                do {

                    MonitorDataModel monitorDataModel = new MonitorDataModel();

                    monitorDataModel.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                    monitorDataModel.setAlpha(cursor.getDouble(cursor.getColumnIndex(KEY_ALPHA)));
                    monitorDataModel.setBeta(cursor.getDouble(cursor.getColumnIndex(KEY_BETA)));
                    monitorDataModel.setDelta(cursor.getDouble(cursor.getColumnIndex(KEY_DELTA)));
                    monitorDataModel.setTheta(cursor.getDouble(cursor.getColumnIndex(KEY_THETA)));
                    monitorDataModel.setTheta_beta_ratio(cursor.getDouble(cursor.getColumnIndex(KEY_THETA_BETA_RATIO)));
                    monitorDataModel.setTheta_alpha_ratio(cursor.getDouble(cursor.getColumnIndex(KEY_THETA_ALPHA_RATIO)));
                    monitorDataModel.setEeg(cursor.getDouble(cursor.getColumnIndex(KEY_EEG)));
                    monitorDataModel.setCreatedAt(cursor.getLong(cursor.getColumnIndex(KEY_TIME_STAMP)));
                    monitorDataModel.setSyncStatus(cursor.getInt(cursor.getColumnIndex(KEY_SYNC_STATUS)));

                    monitorDataModels.add(monitorDataModel);
                } while (cursor.moveToNext());
            }
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return monitorDataModels;
    }

    public ArrayList<MonitorDataModel> getUnSyncMonitoredData() {
        ArrayList<MonitorDataModel> monitorDataModels = new ArrayList<MonitorDataModel>();
        Cursor cursor = null;
        try {
            String sqlquery = "SELECT * FROM " + TABLE_MONITOR_DATA + " WHERE " + KEY_SYNC_STATUS + " = '0'";

            SQLiteDatabase db = this.getReadableDatabase();
            cursor = db.rawQuery(sqlquery, null);

            if (cursor.moveToFirst()) {
                do {

                    MonitorDataModel monitorDataModel = new MonitorDataModel();
                    monitorDataModel.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                    monitorDataModel.setAlpha(cursor.getDouble(cursor.getColumnIndex(KEY_ALPHA)));
                    monitorDataModel.setBeta(cursor.getDouble(cursor.getColumnIndex(KEY_BETA)));
                    monitorDataModel.setDelta(cursor.getDouble(cursor.getColumnIndex(KEY_DELTA)));
                    monitorDataModel.setTheta(cursor.getDouble(cursor.getColumnIndex(KEY_THETA)));
                    monitorDataModel.setTheta_beta_ratio(cursor.getDouble(cursor.getColumnIndex(KEY_THETA_BETA_RATIO)));
                    monitorDataModel.setTheta_alpha_ratio(cursor.getDouble(cursor.getColumnIndex(KEY_THETA_ALPHA_RATIO)));
                    monitorDataModel.setEeg(cursor.getDouble(cursor.getColumnIndex(KEY_EEG)));
                    monitorDataModel.setCreatedAt(cursor.getLong(cursor.getColumnIndex(KEY_TIME_STAMP)));
                    monitorDataModel.setSyncStatus(cursor.getInt(cursor.getColumnIndex(KEY_SYNC_STATUS)));

                    monitorDataModels.add(monitorDataModel);
                } while (cursor.moveToNext());
            }
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return monitorDataModels;
    }

    public ArrayList<MonitorDataModel> get_n_days_monitoredData(long timstamp) {
        ArrayList<MonitorDataModel> monitorDataModels = new ArrayList<MonitorDataModel>();
        Cursor cursor = null;
        try {
            String sqlquery = "SELECT * FROM " + TABLE_MONITOR_DATA + " WHERE " + KEY_TIME_STAMP + " >= " + timstamp;

            SQLiteDatabase db = this.getReadableDatabase();
            cursor = db.rawQuery(sqlquery, null);

            if (cursor.moveToFirst()) {
                do {

                    MonitorDataModel monitorDataModel = new MonitorDataModel();
                    monitorDataModel.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                    monitorDataModel.setAlpha(cursor.getDouble(cursor.getColumnIndex(KEY_ALPHA)));
                    monitorDataModel.setBeta(cursor.getDouble(cursor.getColumnIndex(KEY_BETA)));
                    monitorDataModel.setDelta(cursor.getDouble(cursor.getColumnIndex(KEY_DELTA)));
                    monitorDataModel.setTheta(cursor.getDouble(cursor.getColumnIndex(KEY_THETA)));
                    monitorDataModel.setTheta_beta_ratio(cursor.getDouble(cursor.getColumnIndex(KEY_THETA_BETA_RATIO)));
                    monitorDataModel.setTheta_alpha_ratio(cursor.getDouble(cursor.getColumnIndex(KEY_THETA_ALPHA_RATIO)));
                    monitorDataModel.setEeg(cursor.getDouble(cursor.getColumnIndex(KEY_EEG)));
                    monitorDataModel.setCreatedAt(cursor.getLong(cursor.getColumnIndex(KEY_TIME_STAMP)));
                    monitorDataModel.setSyncStatus(cursor.getInt(cursor.getColumnIndex(KEY_SYNC_STATUS)));

                    monitorDataModels.add(monitorDataModel);
                } while (cursor.moveToNext());
            }
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return monitorDataModels;
    }


    public ArrayList<PointValue> getTehtaBetaRatio() {
        ArrayList<PointValue> thetaBetaRatioes = new ArrayList<PointValue>();
        Cursor cursor = null;
        try {
            String sqlquery = "SELECT * FROM " + TABLE_MONITOR_DATA;

            SQLiteDatabase db = this.getWritableDatabase();
            cursor = db.rawQuery(sqlquery, null);
            float i = 0;
            if (cursor.moveToFirst()) {
                do {
                    float thetaBetaRatio = (float) cursor.getDouble(cursor.getColumnIndex(KEY_THETA_BETA_RATIO));
//                    float TBPercentage = (((10 - thetaBetaRatio) / 10f) * 100) / 10f;
                    PointValue pointValue = new PointValue(i++, thetaBetaRatio);
                    thetaBetaRatioes.add(pointValue);
                } while (cursor.moveToNext());
            }
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return thetaBetaRatioes;
    }

    public ArrayList<PointValue> getTehtaBetaRatio(long timeStamp) {
        ArrayList<PointValue> thetaBetaRatioes = new ArrayList<PointValue>();
        Cursor cursor = null;
        try {
            String sqlquery = "SELECT * FROM " + TABLE_MONITOR_DATA + " WHERE " + KEY_TIME_STAMP + " >= " + timeStamp;

            SQLiteDatabase db = this.getWritableDatabase();
            cursor = db.rawQuery(sqlquery, null);
            float i = 0;
            if (cursor.moveToFirst()) {
                do {
                    float thetaBetaRatio = (float) cursor.getDouble(cursor.getColumnIndex(KEY_THETA_BETA_RATIO));
//                    float TBPercentage = (((10 - thetaBetaRatio) / 10f) * 100) / 10f;
                    PointValue pointValue = new PointValue(i++, thetaBetaRatio);
                    thetaBetaRatioes.add(pointValue);
                } while (cursor.moveToNext());
            }
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return thetaBetaRatioes;
    }

    public ArrayList<PointValue> getTehtaAlphaRatio() {
        ArrayList<PointValue> thetaAlphaRatioes = new ArrayList<PointValue>();
        Cursor cursor = null;
        try {
            String sqlquery = "SELECT * FROM " + TABLE_MONITOR_DATA;

            SQLiteDatabase db = this.getWritableDatabase();
            cursor = db.rawQuery(sqlquery, null);
            float i = 0;
            if (cursor.moveToFirst()) {
                do {
                    float thetaAlphaRatio = (float) cursor.getDouble(cursor.getColumnIndex(KEY_THETA_ALPHA_RATIO));
//                    float TAPercentage = (((5 - thetaAlphaRatio) / 5f) * 100) / 10f;
                    PointValue pointValue = new PointValue(i++, thetaAlphaRatio);
                    thetaAlphaRatioes.add(pointValue);
                } while (cursor.moveToNext());
            }
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return thetaAlphaRatioes;
    }

    public ArrayList<PointValue> getTehtaAlphaRatio(long timeStamp) {
        ArrayList<PointValue> thetaAlphaRatioes = new ArrayList<PointValue>();
        Cursor cursor = null;
        try {
            String sqlquery = "SELECT * FROM " + TABLE_MONITOR_DATA + " WHERE " + KEY_TIME_STAMP + " >= " + timeStamp;

            SQLiteDatabase db = this.getWritableDatabase();
            cursor = db.rawQuery(sqlquery, null);
            float i = 0;
            if (cursor.moveToFirst()) {
                do {
                    float thetaAlphaRatio = (float) cursor.getDouble(cursor.getColumnIndex(KEY_THETA_ALPHA_RATIO));
//                    float TAPercentage = (((5 - thetaAlphaRatio) / 5f) * 100) / 10f;
                    PointValue pointValue = new PointValue(i++, thetaAlphaRatio);
                    thetaAlphaRatioes.add(pointValue);
                } while (cursor.moveToNext());
            }
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return thetaAlphaRatioes;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(CREATE_TABLE_MONITOR_DATA);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_NAME);
        db.execSQL(CREATE_TABLE_MONITOR_DATA);
    }

    public void exportDatabse() {
        try {
            File sd = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "");
            File data = Environment.getDataDirectory();

            if (sd.canWrite()) {
                String currentDBPath = "//data//" + mContext.getPackageName() + "//databases//" + DATABASE_NAME + "";
                String backupDBPath = DATABASE_NAME + ".db";
                File currentDB = new File(data, currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            }
        } catch (Exception e) {

        }
    }

}
