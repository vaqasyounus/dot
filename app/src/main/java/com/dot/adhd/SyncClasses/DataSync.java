package com.dot.adhd.SyncClasses;

import android.content.Context;

import com.dot.adhd.helpers.DatabaseHelper;
import com.dot.adhd.models.AppConstant;
import com.dot.adhd.models.AppModel;
import com.dot.adhd.models.MonitorDataModel;
import com.dot.adhd.models.PointModel;
import com.dot.adhd.retrofit.ApiClient;
import com.dot.adhd.retrofit.ApiInterface;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Response;

/**
 * Created by Muhammad Waqas on 5/11/2017.
 */

public class DataSync {

    Context context;
    String apiKey;
    public static boolean isSyncRunning = false;

    public DataSync(Context context) {
        this.context = context;
        this.apiKey = AppModel.getInstance().readFromSharedPrefrence(context, AppConstant.API_KEY_SP);
    }

    public void SyncPoint() {
        PointModel pointModel = new PointModel();
        ArrayList<MonitorDataModel> monitorDataModels = DatabaseHelper.getInstance(context).getUnSyncMonitoredData();
        pointModel.setPoints(monitorDataModels);
        for (int i = 0; i < pointModel.getPoints().size(); i++) {
            Map<String, Object> data = new HashMap<>();
            data.put("alpha", pointModel.getPoints().get(i).getAlpha());
            data.put("beta", pointModel.getPoints().get(i).getBeta());
            data.put("delta", pointModel.getPoints().get(i).getDelta());
            data.put("theta", pointModel.getPoints().get(i).getTheta());
            data.put("theta_beta_ratio", pointModel.getPoints().get(i).getTheta_beta_ratio());
            data.put("theta_alpha_ratio", pointModel.getPoints().get(i).getTheta_alpha_ratio());
            data.put("eeg", pointModel.getPoints().get(i).getEeg());
            data.put("createdAt", pointModel.getPoints().get(i).getCreatedAt());
            try {
                Response<PointModel> rpm = ApiClient.getClient().create(ApiInterface.class).postPoint(apiKey, data).execute();
                PointModel pm = rpm.body();
                if (rpm.isSuccessful() && !pm.isError()) {
                    DatabaseHelper.getInstance(context).updateMonitorData(pointModel.getPoints().get(i).getId());
                }


            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


}
