package com.dot.adhd.retrofit;

import com.dot.adhd.models.AppConstant;
import com.dot.adhd.models.MonitorDataModel;
import com.dot.adhd.models.PointModel;
import com.dot.adhd.models.UserModel;

import java.util.List;
import java.util.Map;


import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface ApiInterface {

    @FormUrlEncoded
    @POST(AppConstant.URL_LOGIN)
    Call<UserModel> loginAPI(@FieldMap(encoded = true) Map<String, String> postData);

    @FormUrlEncoded
    @POST(AppConstant.URL_REGISTER)
    Call<UserModel> registerAPI(@FieldMap(encoded = true) Map<String, String> postData);


    @FormUrlEncoded
    @POST(AppConstant.URL_POINTS)
    Call<PointModel> postPoint(@Header("Authorization") String apiKey,@FieldMap(encoded = true) Map<String, Object> postData);


    @GET(AppConstant.URL_POINTS)
    Call<PointModel> getPoints(@Header("Authorization") String apiKey);

}
