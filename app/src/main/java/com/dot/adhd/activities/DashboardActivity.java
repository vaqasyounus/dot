package com.dot.adhd.activities;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dot.adhd.R;
import com.dot.adhd.SyncClasses.SyncUtils;
import com.dot.adhd.helpers.DatabaseHelper;
import com.dot.adhd.models.AppConstant;
import com.dot.adhd.models.AppModel;
import com.dot.adhd.models.MonitorDataModel;
import com.dot.adhd.models.PointModel;
import com.dot.adhd.retrofit.ApiClient;
import com.dot.adhd.retrofit.ApiInterface;
import com.github.hujiaweibujidao.wava.Techniques;
import com.github.hujiaweibujidao.wava.YoYo;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardActivity extends AppCompatActivity {
    private ImageView monitor;

    private String apiKey;
    private RelativeLayout monitor_bubble;
    private RelativeLayout games_bubble;
    private RelativeLayout progress_bubble;
    private RelativeLayout attention_bubble;
    private TextView name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ensurePermissions();
        Toolbar toolbar = (Toolbar) findViewById(R.id.id_toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        SyncUtils.CreateSyncAccount(this);
        SyncUtils.TriggerRefresh(new Bundle());
        this.apiKey = AppModel.getInstance().readFromSharedPrefrence(getApplicationContext(), AppConstant.API_KEY_SP);
        String isGetData = AppModel.getInstance().readFromSharedPrefrence(getApplicationContext(), AppConstant.IS_GET_DATA_SP);
        if (isGetData.equals("false")) {
            ApiClient.getClient().create(ApiInterface.class).getPoints(apiKey).enqueue(new Callback<PointModel>() {
                @Override
                public void onResponse(Call<PointModel> call, Response<PointModel> response) {
                    if (response.isSuccessful() && !response.body().isError()) {
                        ArrayList<MonitorDataModel> mdmL = response.body().getPoints();
                        for (int j = 0; j < mdmL.size(); j++) {
                            MonitorDataModel mdm = mdmL.get(j);
                            mdm.setSyncStatus(1);
                            DatabaseHelper.getInstance(DashboardActivity.this).addMonitorData(mdm);
                        }
                    }
                }

                @Override
                public void onFailure(Call<PointModel> call, Throwable t) {

                }
            });
            AppModel.getInstance().writeToSharedPrefrence(getApplicationContext(), AppConstant.IS_GET_DATA_SP, "true");
        }
        games_bubble = (RelativeLayout) findViewById(R.id.games_bubble);
        name = (TextView) findViewById(R.id.name);
        name.setText("Hi " + AppModel.getInstance().readFromSharedPrefrence(getApplicationContext(), AppConstant.KEY_SP_Name));
        progress_bubble = (RelativeLayout) findViewById(R.id.progress_bubble);
        //monitor = (ImageView) findViewById(R.id.monitor);
        monitor_bubble = (RelativeLayout) findViewById(R.id.monitor_bubble);
        attention_bubble = (RelativeLayout) findViewById(R.id.attension_bubble);

        games_bubble.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DashboardActivity.this, GamesActivity.class));
            }
        });
        monitor_bubble.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DashboardActivity.this, ResultActivity.class));
            }
        });
        progress_bubble.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DashboardActivity.this, ProgressActivity.class));
            }
        });

        YoYo.with(Techniques.Pulse).duration(1500)
                .interpolate(new AccelerateDecelerateInterpolator())
                .listen(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        YoYo.with(Techniques.Pulse).duration(1500)
                                .interpolate(new AccelerateDecelerateInterpolator())
                                .listen(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        super.onAnimationEnd(animation);
                                        YoYo.with(Techniques.Pulse).duration(1500)
                                                .interpolate(new AccelerateDecelerateInterpolator())
                                                .listen(new AnimatorListenerAdapter() {
                                                    @Override
                                                    public void onAnimationEnd(Animator animation) {
                                                        super.onAnimationEnd(animation);
                                                        YoYo.with(Techniques.Pulse).duration(1500)
                                                                .interpolate(new AccelerateDecelerateInterpolator())
                                                                .listen(new AnimatorListenerAdapter() {
                                                                    @Override
                                                                    public void onAnimationEnd(Animator animation) {
                                                                        super.onAnimationEnd(animation);
                                                                    }
                                                                })
                                                                .playOn(progress_bubble);
                                                    }
                                                })
                                                .playOn(attention_bubble);
                                    }
                                })
                                .playOn(games_bubble);
                    }
                })
                .playOn(monitor_bubble);

    }

    @Override
    protected void onResume() {
        super.onResume();
        BluetoothAdapter.getDefaultAdapter().enable();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.dashboard_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.signout) {
            AppModel.getInstance().removeSharedPrefrence(getApplicationContext());
            DatabaseHelper.getInstance(DashboardActivity.this).deleteAllData();
            Intent intent = new Intent(this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    volatile int i = 0;

    private void blink(final View relativeLayout) {
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                int timeToBlink = 500;    //in milissegunds
                try {
                    Thread.sleep(timeToBlink);
                } catch (Exception e) {
                }
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (relativeLayout.getVisibility() == View.VISIBLE) {
                            relativeLayout.setVisibility(View.INVISIBLE);
                        } else {
                            relativeLayout.setVisibility(View.VISIBLE);
                        }
                        i++;
                        if (i <= 1) {
                            blink(relativeLayout);
                        } else if (i <= 3) {
                            blink(games_bubble);
                        } else if (i <= 5) {
                            blink(progress_bubble);
                        }
                    }
                });
            }
        }).start();
    }


    public void goToMonitor(View view) {
        startActivity(new Intent(DashboardActivity.this, ResultActivity.class));
    }

    public void goToGames(View view) {
        startActivity(new Intent(DashboardActivity.this, GamesActivity.class));
    }

    public void goToProgress(View view) {
        startActivity(new Intent(DashboardActivity.this, ProgressActivity.class));
    }

    public void goToAttension(View view) {
        startActivity(new Intent(DashboardActivity.this, AttentionActivity.class));
    }
    private void ensurePermissions() {

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // We don't have the ACCESS_COARSE_LOCATION permission so create the dialogs asking
            // the user to grant us the permission.

            DialogInterface.OnClickListener buttonListener =
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            ActivityCompat.requestPermissions(DashboardActivity.this,
                                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                                    0);
                        }
                    };

            // This is the context dialog which explains to the user the reason we are requesting
            // this permission.  When the user presses the positive (I Understand) button, the
            // standard Android permission dialog will be displayed (as defined in the button
            // listener above).
            AlertDialog introDialog = new AlertDialog.Builder(this)
                    .setTitle(R.string.permission_dialog_title)
                    .setMessage(R.string.permission_dialog_description)
                    .setPositiveButton(R.string.permission_dialog_understand, buttonListener)
                    .create();
            introDialog.show();
        }
    }

}
