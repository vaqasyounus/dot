package com.dot.adhd.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.dot.adhd.R;
import com.dot.adhd.SyncClasses.SyncUtils;
import com.dot.adhd.helpers.DatabaseHelper;
import com.dot.adhd.models.AppConstant;
import com.dot.adhd.models.AppModel;
import com.dot.adhd.models.MonitorDataModel;
import com.dot.adhd.models.UserModel;
import com.dot.adhd.retrofit.ApiClient;
import com.dot.adhd.retrofit.ApiInterface;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText userName;
    private EditText password;
    private Button signIn;
    private RelativeLayout parentLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        userName = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        signIn = (Button) findViewById(R.id.signIn);
        parentLayout = (RelativeLayout) findViewById(R.id.parentLayout);
        signIn.setOnClickListener(this);
        password.setTransformationMethod(new PasswordTransformationMethod());
    }

    public void register(View v) {
        startActivity(new Intent(this, RegisterActivity.class));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.signIn:
                if (AppModel.getInstance().isValid(parentLayout)) {

                    final AlertDialog dialog = new SpotsDialog(LoginActivity.this);
                    dialog.show();
                    Map<String, String> data = new HashMap<>();
                    data.put("email", userName.getText().toString());
                    data.put("password", password.getText().toString());
                    ApiClient.getClient().create(ApiInterface.class).loginAPI(data).enqueue(new Callback<UserModel>() {
                        @Override
                        public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                            dialog.dismiss();
                            if (response.isSuccessful() && response.body().isError() == false) {
                                AppModel.getInstance().writeToSharedPrefrence(getApplicationContext(), AppConstant.API_KEY_SP, response.body().getApiKey());
                                AppModel.getInstance().writeToSharedPrefrence(getApplicationContext(), AppConstant.KEY_SP_Name, response.body().getName());

                                startActivity(new Intent(LoginActivity.this, DashboardActivity.class));
                                finish();
                            } else {

                                AppModel.getInstance().popUpMessage(LoginActivity.this, response.body().getMessage());
                            }
                        }

                        @Override
                        public void onFailure(Call<UserModel> call, Throwable t) {
                            dialog.dismiss();
                            AppModel.getInstance().popUpMessage(LoginActivity.this, "Network failure");
                        }
                    });
                }
        }
    }
}
