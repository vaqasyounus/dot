package com.dot.adhd.activities;

import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dot.adhd.R;
import com.dot.adhd.helpers.DatabaseHelper;
import com.dot.adhd.models.AppModel;

import java.util.ArrayList;
import java.util.List;

import belka.us.androidtoggleswitch.widgets.BaseToggleSwitch;
import belka.us.androidtoggleswitch.widgets.ToggleSwitch;
import lecho.lib.hellocharts.listener.LineChartOnValueSelectListener;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.view.LineChartView;

public class ProgressActivity extends BottomBarActivity {

    LineChartView chart_thetaAlpha;
    LineChartView chart_thetaBeta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.frame_main);
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View screen = layoutInflater.inflate(R.layout.activity_progress, null, false);
        frameLayout.addView(screen);
        AppModel.getInstance().currentActivity = ProgressActivity.this;
        progress.setBackgroundColor(ContextCompat.getColor(this, R.color.light_yellow));
        progress.setTextColor(ContextCompat.getColor(this, R.color.colorWhite));
        TextView tabTitle = (TextView) findViewById(R.id.tab_title);
        tabTitle.setText("Progress");
        ImageView tabIcon = (ImageView) findViewById(R.id.tab_icon);
        tabIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProgressActivity.this, DashboardActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        chart_thetaAlpha = (LineChartView) findViewById(R.id.chart_theta_alpha);
        chart_thetaBeta = (LineChartView) findViewById(R.id.chart_theta_beta);

        chart_thetaBeta.setOnValueTouchListener(new LineChartOnValueSelectListener() {
            @Override
            public void onValueSelected(int i, int i1, PointValue pointValue) {
                AppModel.getInstance().popUpMessage(ProgressActivity.this, "T/B Ratio : " + pointValue.getY(), 0);
            }

            @Override
            public void onValueDeselected() {

            }
        });

        chart_thetaAlpha.setOnValueTouchListener(new LineChartOnValueSelectListener() {
            @Override
            public void onValueSelected(int i, int i1, PointValue pointValue) {
                AppModel.getInstance().popUpMessage(ProgressActivity.this, "T/A Ratio : " + pointValue.getY(), 0);
            }

            @Override
            public void onValueDeselected() {

            }
        });


        List<PointValue> thetaBetaRatios = DatabaseHelper.getInstance(this).getTehtaBetaRatio(AppModel.getInstance().previous_n_days_milli_scond(1));
        List<PointValue> thetaAlphaRatios = DatabaseHelper.getInstance(this).getTehtaAlphaRatio(AppModel.getInstance().previous_n_days_milli_scond(1));

        AppModel.getInstance().drawGraph(this, thetaAlphaRatios, chart_thetaAlpha);
        AppModel.getInstance().drawGraph(this, thetaBetaRatios, chart_thetaBeta);


        ToggleSwitch toggleSwitch = (ToggleSwitch) findViewById(R.id.switches);
        toggleSwitch.setToggleWidth((float) 140);
        ArrayList<String> labels = new ArrayList<>();
        labels.add("Day");
        labels.add("Week");
        labels.add("Month");
        labels.add("4 Months");
        labels.add("8 Months");
        labels.add("Year");
        toggleSwitch.setLabels(labels);
        toggleSwitch.setOnToggleSwitchChangeListener(new BaseToggleSwitch.OnToggleSwitchChangeListener() {
            @Override
            public void onToggleSwitchChangeListener(int position, boolean isChecked) {
                long timeStamp = 0;
                switch (position) {
                    case 0:
                        timeStamp = AppModel.getInstance().previous_n_days_milli_scond(1);
                        break;
                    case 1:
                        timeStamp = AppModel.getInstance().previous_n_days_milli_scond(7);
                        break;
                    case 2:
                        timeStamp = AppModel.getInstance().previous_n_days_milli_scond(30);
                        break;
                    case 3:
                        timeStamp = AppModel.getInstance().previous_n_days_milli_scond(120);
                        break;
                    case 4:
                        timeStamp = AppModel.getInstance().previous_n_days_milli_scond(240);
                        break;
                    case 5:
                        timeStamp = AppModel.getInstance().previous_n_days_milli_scond(365);
                        break;
                }

                List<PointValue> thetaBetaRatios = DatabaseHelper.getInstance(ProgressActivity.this).getTehtaBetaRatio(timeStamp);
                List<PointValue> thetaAlphaRatios = DatabaseHelper.getInstance(ProgressActivity.this).getTehtaAlphaRatio(timeStamp);
                AppModel.getInstance().drawGraph(ProgressActivity.this, thetaAlphaRatios, chart_thetaAlpha);
                AppModel.getInstance().drawGraph(ProgressActivity.this, thetaBetaRatios, chart_thetaBeta);
            }
        });
    }



}
