package com.dot.adhd.activities;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.choosemuse.libmuse.ConnectionState;
import com.choosemuse.libmuse.Eeg;
import com.choosemuse.libmuse.LibmuseVersion;
import com.choosemuse.libmuse.Muse;
import com.choosemuse.libmuse.MuseArtifactPacket;
import com.choosemuse.libmuse.MuseConnectionListener;
import com.choosemuse.libmuse.MuseConnectionPacket;
import com.choosemuse.libmuse.MuseDataListener;
import com.choosemuse.libmuse.MuseDataPacket;
import com.choosemuse.libmuse.MuseListener;
import com.choosemuse.libmuse.MuseManagerAndroid;
import com.choosemuse.libmuse.MusePreset;
import com.dot.adhd.R;
import com.dot.adhd.helpers.DatabaseHelper;
import com.dot.adhd.models.AppModel;
import com.dot.adhd.models.MonitorDataModel;
import com.kyleduo.switchbutton.SwitchButton;
import com.pascalwelsch.holocircularprogressbar.HoloCircularProgressBar;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import static com.choosemuse.libmuse.MuseDataPacketType.ALPHA_RELATIVE;
import static com.choosemuse.libmuse.MuseDataPacketType.BATTERY;
import static com.choosemuse.libmuse.MuseDataPacketType.BETA_RELATIVE;
import static com.choosemuse.libmuse.MuseDataPacketType.DELTA_RELATIVE;
import static com.choosemuse.libmuse.MuseDataPacketType.EEG;
import static com.choosemuse.libmuse.MuseDataPacketType.THETA_RELATIVE;

public class ResultActivity extends BottomBarActivity {
    private final String TAG = "TestLibMuseAndroid";
    private MuseManagerAndroid manager;
    private Muse muse;
    private ResultActivity.ConnectionListener connectionListener;
    private ResultActivity.DataListener dataListener;
    private final double[] eegBuffer = new double[6];
    private boolean eegStale;
    private final double[] alphaBuffer = new double[6];
    private boolean alphaStale;
    private final double[] accelBuffer = new double[3];
    private boolean accelStale;

    /**
     * We will be updating the UI using a handler instead of in packet handlers because
     * packets come in at a very high frequency and it only makes sense to update the UI
     * at about 60fps. The update functions do some string allocation, so this reduces our memory
     * footprint and makes GC pauses less frequent/noticeable.
     */
    private final Handler handler = new Handler();

    private boolean dataTransmission = true;


    long MillisecondTime, StartTime, TimeBuff, UpdateTime = 0L;
    int Seconds, Minutes, MilliSeconds;
    ArrayAdapter<String> museListAdapter;
    Dialog connectDialog;
    volatile boolean[] isSensorOn = new boolean[4];
    TextView connect_tv;
    boolean isBatteryBelow;
    boolean isSensorActivate;
    double TBPercentage;
    double TAPercentage;
    boolean mAnimationHasEnded;


    private double currAlpha = 0d;
    private double currBeta = 0d;
    private double currDelta = 0d;
    private double currTheta = 0d;
    private double thetaBetaRatio = 0d;
    private double thetaAlphaRatio = 0d;
    int sensorPosition;

    View sensor1;
    View sensor2;
    View sensor3;
    View sensor4;

    TextView proceed;

    private ObjectAnimator objectAnimator;
    private int progressColor;
    private int animationCount = 0;
    private HoloCircularProgressBar progressBar;
    private HoloCircularProgressBar progressBar2;
    Dialog dialogdetail;
    TextView start;
    TextView discription;
    TextView concentrationPercentage;
    TextView relaxationPercentage;
    TextView detail;


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.frame_main);
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View screen = layoutInflater.inflate(R.layout.activity_result, null, false);
        frameLayout.addView(screen);
        AppModel.getInstance().currentActivity = ResultActivity.this;

        monitor.setBackgroundColor(ContextCompat.getColor(this, R.color.light_yellow));
        monitor.setTextColor(ContextCompat.getColor(this, R.color.colorWhite));

        TextView tabTitle = (TextView) findViewById(R.id.tab_title);
        tabTitle.setText("Snapshot");
        ImageView tabIcon = (ImageView) findViewById(R.id.tab_icon);
        tabIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ResultActivity.this, DashboardActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        museListAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_single_choice);


        manager = MuseManagerAndroid.getInstance();
        manager.setContext(this);
        Log.i(TAG, "LibMuse version=" + LibmuseVersion.instance().getString());
        WeakReference<ResultActivity> weakActivity =
                new WeakReference<ResultActivity>(this);
        connectionListener = new ResultActivity.ConnectionListener(weakActivity);
        dataListener = new ResultActivity.DataListener(weakActivity);
        manager.setMuseListener(new ResultActivity.MuseL(weakActivity));
        manager.startListening();

        start = (TextView) findViewById(R.id.start);
        concentrationPercentage = (TextView) findViewById(R.id.concentration);
        relaxationPercentage = (TextView) findViewById(R.id.relaxation);
        discription = (TextView) findViewById(R.id.discription);
        detail = (TextView) findViewById(R.id.detail);
        progressBar = (HoloCircularProgressBar) findViewById(R.id.progress_bar);
        progressBar2 = (HoloCircularProgressBar) findViewById(R.id.progress_bar2);
        progressBar.setProgressColor(Color.GREEN);
        progressBar.setProgressBackgroundColor(Color.WHITE);
        progressBar2.setProgressBackgroundColor(Color.WHITE);
        progressBar2.setProgressColor(Color.GREEN);
        progressColor = progressBar.getProgressColor();

        detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                detailDialog(currTheta, currAlpha, currBeta, currDelta, thetaBetaRatio, thetaAlphaRatio);
            }
        });
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start.setClickable(false);
                animationCount = 0;
                mAnimationHasEnded = false;
                currAlpha = 0d;
                currBeta = 0d;
                currDelta = 0d;
                currTheta = 0d;
                thetaBetaRatio = 0d;
                thetaAlphaRatio = 0d;
                TBPercentage = 0d;
                TAPercentage = 0d;
                relaxationPercentage.setText("0 %");
                concentrationPercentage.setText("0 %");
                discription.setText("");

                animate(ResultActivity.this.progressBar, new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        ResultActivity.this.animationCount++;
                        if (animationCount <= 30) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    start.setText(animationCount + "");
                                }
                            });
                        }
                        if (animationCount >= 29 && animationCount <= 30) {
                            TBPercentage = ((10 - thetaBetaRatio) / 10d) * 100;
                            TAPercentage = ((5 - thetaAlphaRatio) / 5d) * 100;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    start.setClickable(true);
                                    start.setText("Start");
                                    if (((TBPercentage >= 0 && TBPercentage <= 100) && (TAPercentage >= 0 && TAPercentage <= 100))) {
                                        concentrationPercentage.setText(String.format("%.2f", TBPercentage) + " %");
                                        relaxationPercentage.setText(String.format("%.2f", TAPercentage) + " %");

                                        DatabaseHelper.getInstance(ResultActivity.this).addMonitorData(new MonitorDataModel(currTheta, currBeta, currAlpha, currDelta, thetaBetaRatio, thetaAlphaRatio, 0.0, System.currentTimeMillis(), 0));

                                        if (TBPercentage < 60 && TAPercentage < 60) {
                                            discription.setText("Consult your doctor \n your brain activity is not optimal.");
                                        } else if (TAPercentage <= 60) {
                                            discription.setText("You seem to be stressed, try to relax.");
                                        } else if (TBPercentage <= 60) {
                                            discription.setText("STAY FOCUSED \n you seem to be daydreaming.");
                                        } else if (TAPercentage >= 60 && TAPercentage <= 65) {
                                            discription.setText("CHILL OUT \n you're bordering on being stressed.");
                                        } else if (TBPercentage >= 60 && TBPercentage <= 65) {
                                            discription.setText("I can see you are losing focus...");
                                        } else {
                                            discription.setText("Your brain activity looks great, keep up the good work!");
                                        }

                                    } else {
                                        discription.setText("Whoops! something went wrong please try again.");
                                        finish();
                                    }
                                }
                            });
                        }
                        if (!mAnimationHasEnded) {
                            animate(progressBar, this, ResultActivity.this.thetaBetaRatio);
                        } else {
                            animation.cancel();
                        }

                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                }, ResultActivity.this.thetaBetaRatio);

                animate(ResultActivity.this.progressBar2, new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        if (!mAnimationHasEnded) {
                            animate(progressBar2, this, ResultActivity.this.thetaAlphaRatio);
                        } else {
                            animation.cancel();
                        }

                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                }, ResultActivity.this.thetaAlphaRatio);


            }
        });

        showDialog();

    }

    @Override
    protected void onPause() {
        super.onPause();
        manager.stopListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (muse != null)
            muse.disconnect();
    }

    public void museListChanged() {
        final List<Muse> list = manager.getMuses();
        museListAdapter.clear();
        for (Muse m : list) {
            museListAdapter.add(m.getName() + " - " + m.getMacAddress());
        }
    }

    public void receiveMuseConnectionPacket(final MuseConnectionPacket p, final Muse muse) {

        final ConnectionState current = p.getCurrentConnectionState();

        // Format a message to show the change of connection state in the UI.
        final String status = p.getPreviousConnectionState() + " -> " + current;
        Log.i(TAG, status);

        // Update the UI with the change in connection state.
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (current == ConnectionState.CONNECTED) {
                    connect_tv.setText(current.name());
                    AppModel.getInstance().popUpMessage(ResultActivity.this, "Waiting for sensors to get activated");
                } else if (current == ConnectionState.DISCONNECTED) {
                    AppModel.getInstance().popUpMessage(ResultActivity.this, "Headset not found");
                    connect_tv.setText(current.name());
                    /*if (!connectDialog.isShowing()) {
                        connectDialog.show();
                    }*/
                } else {
                    connect_tv.setText(current.name());
                    AppModel.getInstance().popUpMessage(ResultActivity.this, "connecting to your headset");
                }
            }
        });

        if (current == ConnectionState.DISCONNECTED) {
            Log.i(TAG, "Muse disconnected:" + muse.getName());
            // We have disconnected from the headband, so set our cached copy to null.
            this.muse = null;
        }
    }

    public void receiveMuseDataPacket(final MuseDataPacket p, final Muse muse) {
        // valuesSize returns the number of data values contained in the packet.
        handler.post(new Runnable() {
            @Override
            public void run() {
                try {
                    ArrayList<Double> data = new ArrayList<>();
                    data.add(p.values().get(1));
                    data.add(p.values().get(2));
                    data.add(p.values().get(3));
                    data.add(p.values().get(4));

                    if (p.packetType() == ALPHA_RELATIVE || p.packetType() == EEG || p.packetType() == BETA_RELATIVE || p.packetType() == THETA_RELATIVE || p.packetType() == DELTA_RELATIVE) {
                        for (int i = 0; i < data.size(); i++)
                            isSensorOn[i] = !data.get(i).isNaN();
                        int count = 0;
                        for (sensorPosition = 0; sensorPosition < isSensorOn.length; sensorPosition++) {


                            if (isSensorOn[sensorPosition]) {
                                count++;

                                switch (sensorPosition) {
                                    case 0:
                                        sensor1.setBackgroundColor(Color.GREEN);
                                        break;
                                    case 1:
                                        sensor2.setBackgroundColor(Color.GREEN);
                                        break;
                                    case 2:
                                        sensor3.setBackgroundColor(Color.GREEN);
                                        break;
                                    case 3:
                                        sensor4.setBackgroundColor(Color.GREEN);
                                        break;

                                }

                            } else {
                                switch (sensorPosition) {
                                    case 0:
                                        sensor1.setBackgroundColor(Color.GRAY);
                                        break;
                                    case 1:
                                        sensor2.setBackgroundColor(Color.GRAY);
                                        break;
                                    case 2:
                                        sensor3.setBackgroundColor(Color.GRAY);
                                        break;
                                    case 3:
                                        sensor4.setBackgroundColor(Color.GRAY);
                                        break;

                                }

                            }
                        }
                        if (count >= 2) {

                            if (!isSensorActivate) {
                                isSensorActivate = true;
                                if (connectDialog.isShowing()) {
                                    proceed.setTextColor(Color.GREEN);
                                    AppModel.getInstance().popUpMessage(ResultActivity.this, "Sensors activated tap to proceed");
                                } else {
                                    AppModel.getInstance().popUpMessage(ResultActivity.this, "Sensors activated again");
                                }
                            }

                        } else {

                            if (isSensorActivate) {
                                isSensorActivate = false;
                                if (!connectDialog.isShowing()) {
                                    connectDialog.show();
                                    AppModel.getInstance().popUpMessage(ResultActivity.this, "Sensors deactivated adjust your device");
                                    proceed.setTextColor(ContextCompat.getColor(ResultActivity.this, R.color.colorBlackTransparent));
                                } else {
                                    AppModel.getInstance().popUpMessage(ResultActivity.this, "Sensors deactivated adjust your device");
                                    proceed.setTextColor(ContextCompat.getColor(ResultActivity.this, R.color.colorBlackTransparent));

                                }
                            }

                        }
                    }
                } catch (Exception e) {
                    Log.d("Exception", "");
                }
                if (ResultActivity.this.animationCount >= 29) {
                    mAnimationHasEnded = true;
                    muse.enableDataTransmission(false);

                }
                switch (p.packetType()) {
                    case EEG:
                        updateEeg(p.values());
                        break;
                    case ALPHA_RELATIVE:
                        updateAlphaRelative(p.values());
                        break;
                    case BETA_RELATIVE:
                        updateBetaRelative(p.values());
                        break;
                    case THETA_RELATIVE:
                        updateThetaRelative(p.values());
                        break;
                    case DELTA_RELATIVE:
                        updateDeltaRelative(p.values());
                        break;
                    case ACCELEROMETER:
                        updateAccelerometer(p.values());
                        break;
                    case BATTERY:
                        updateBattery(p.values());
                        break;
                    default:
                        break;
                }
            }
        });
    }

    public void receiveMuseArtifactPacket(final MuseArtifactPacket p, final Muse muse) {

        handler.post(new Runnable() {
            @Override
            public void run() {
               /* if (!p.getHeadbandOn()) {
                    connect_tv.setText("Put headband on your forhead");
                }else{
                    connect_tv.setText("Connected");
                }*/
            }
        });

    }

    class MuseL extends MuseListener {
        final WeakReference<ResultActivity> activityRef;

        MuseL(final WeakReference<ResultActivity> activityRef) {
            this.activityRef = activityRef;
        }

        @Override
        public void museListChanged() {
            activityRef.get().museListChanged();
        }
    }

    class ConnectionListener extends MuseConnectionListener {
        final WeakReference<ResultActivity> activityRef;

        ConnectionListener(final WeakReference<ResultActivity> activityRef) {
            this.activityRef = activityRef;
        }

        @Override
        public void receiveMuseConnectionPacket(final MuseConnectionPacket p, final Muse muse) {
            activityRef.get().receiveMuseConnectionPacket(p, muse);
        }
    }

    class DataListener extends MuseDataListener {
        final WeakReference<ResultActivity> activityRef;

        DataListener(final WeakReference<ResultActivity> activityRef) {
            this.activityRef = activityRef;
        }

        @Override
        public void receiveMuseDataPacket(final MuseDataPacket p, final Muse muse) {
            activityRef.get().receiveMuseDataPacket(p, muse);
        }

        @Override
        public void receiveMuseArtifactPacket(final MuseArtifactPacket p, final Muse muse) {
            activityRef.get().receiveMuseArtifactPacket(p, muse);
        }
    }

    public void showDialog() {
        final BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        connectDialog = new Dialog(this);
        connectDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        connectDialog.setContentView(R.layout.dialog_layout);
        SwitchButton status = (SwitchButton) connectDialog.findViewById(R.id.status);
        TextView scan = (TextView) connectDialog.findViewById(R.id.scan);
        final ListView musesList = (ListView) connectDialog.findViewById(R.id.muses_list);
        final TextView connect = (TextView) connectDialog.findViewById(R.id.connect);
        proceed = (TextView) connectDialog.findViewById(R.id.proceed);
        TextView cancel = (TextView) connectDialog.findViewById(R.id.cancel);
        status.setChecked(bluetoothAdapter.isEnabled());
        status.setClickable(false);
        proceed.setTextColor(ContextCompat.getColor(ResultActivity.this, R.color.colorBlackTransparent));
        connect_tv = (TextView) connectDialog.findViewById(R.id.status_tv);
        sensor1 = connectDialog.findViewById(R.id.sensor1);
        sensor2 = connectDialog.findViewById(R.id.sensor2);
        sensor3 = connectDialog.findViewById(R.id.sensor3);
        sensor4 = connectDialog.findViewById(R.id.sensor4);

        musesList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        musesList.setAdapter(museListAdapter);

        SparseBooleanArray checkedItemPositions = musesList.getCheckedItemPositions();
        if (checkedItemPositions.size() > 0) {
            connect.setTextColor(ContextCompat.getColor(ResultActivity.this, R.color.colorDarkBlue1));
        } else {
            connect.setTextColor(ContextCompat.getColor(ResultActivity.this, R.color.colorBlackTransparent));
        }
        musesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long id) {
                musesList.setItemChecked(i, true);
                connect.setTextColor(ContextCompat.getColor(ResultActivity.this, R.color.colorDarkBlue1));
                connect.setText("Connect");
                muse = manager.getMuses().get(i);
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (muse != null)
                    muse.disconnect();
                finish();
            }
        });
        scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manager.stopListening();
                manager.startListening();
            }
        });
        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = 0;
                for (int i = 0; i < isSensorOn.length; i++) {
                    if (isSensorOn[i]) {
                        count++;
                    }
                }
                if (count >= 2) {
                    connectDialog.dismiss();
                } else {
                    AppModel.getInstance().popUpMessage(ResultActivity.this, "Sensors are still inactive");
                }


            }
        });
        connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!bluetoothAdapter.isEnabled()) {
                    AppModel.getInstance().popUpMessage(ResultActivity.this, "Turn ON your bluetooth");
                    return;
                }
//                muse.unregisterAllListeners();
                muse.registerConnectionListener(connectionListener);
                muse.registerDataListener(dataListener, ALPHA_RELATIVE);
                muse.registerDataListener(dataListener, BETA_RELATIVE);
                muse.registerDataListener(dataListener, THETA_RELATIVE);
                muse.registerDataListener(dataListener, DELTA_RELATIVE);
                muse.registerDataListener(dataListener, BATTERY);
//                muse.registerDataListener(dataListener, ARTIFACTS);
                muse.setPreset(MusePreset.PRESET_14);

                muse.runAsynchronously();

            }
        });
        connectDialog.setCancelable(false);

        connectDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        connectDialog.show();
    }

    void detailDialog(double thetaV, double alphaV, double betaV, double deltaV, double tbrV, double tarV) {

        dialogdetail = new Dialog(this);
        dialogdetail.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogdetail.setContentView(R.layout.detail_dialog_layout);
        dialogdetail.setCancelable(true);
        TextView theta = (TextView) dialogdetail.findViewById(R.id.theta);
        TextView alpha = (TextView) dialogdetail.findViewById(R.id.alpha);
        TextView beta = (TextView) dialogdetail.findViewById(R.id.beta);
        TextView delta = (TextView) dialogdetail.findViewById(R.id.delta);

        TextView tbr = (TextView) dialogdetail.findViewById(R.id.tbr);
        TextView tar = (TextView) dialogdetail.findViewById(R.id.tar);

        theta.setText(String.format("%.4f", thetaV));
        alpha.setText(String.format("%.4f", alphaV));
        beta.setText(String.format("%.4f", betaV));
        delta.setText(String.format("%.4f", deltaV));
        tbr.setText(String.format("%.4f", tbrV));
        tar.setText(String.format("%.4f", tarV));
        dialogdetail.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogdetail.show();
    }

    void updateDeltaRelative(final ArrayList<Double> data) {
        Log.v("datatype", "delta " + ResultActivity.this.currDelta + " " + data);
        double temp = (data.get(Eeg.EEG2.ordinal()) + data.get(Eeg.EEG2.ordinal())) / 2d;
        if (Double.isNaN(temp)) return;
        else if (ResultActivity.this.currDelta == 0d)
            ResultActivity.this.currDelta = temp;
        else {
            ResultActivity.this.currDelta = (ResultActivity.this.currDelta + temp) / 2d;
        }

    }

    void updateBetaRelative(final ArrayList<Double> data) {


        Log.v("datatype", "beta " + ResultActivity.this.currBeta + " " + data);
        double temp = (data.get(Eeg.EEG2.ordinal())
                + data.get(Eeg.EEG2.ordinal())) / 2d;
        if (Double.isNaN(temp)) return;
        else if (ResultActivity.this.currBeta == 0d)
            ResultActivity.this.currBeta = temp;
        else {
            ResultActivity.this.currBeta = (ResultActivity.this.currBeta + temp) / 2d;


        }
    }

    void updateThetaRelative(final ArrayList<Double> data) {
        //we're only interested in getting data from the 2 forehead sensors
        Log.v("datatype", "theta " + ResultActivity.this.currTheta + " " + data);
        double temp = (data.get(Eeg.EEG2.ordinal())
                + data.get(Eeg.EEG2.ordinal())) / 2d;
        if (Double.isNaN(temp)) return;
        else if (ResultActivity.this.currTheta == 0d)
            ResultActivity.this.currTheta = temp;
        else {
            ResultActivity.this.currTheta = (ResultActivity.this.currTheta + temp) / 2d;
        }

        double thetaBeta = currTheta / currBeta;
        Log.v("ratios", currTheta + " " + currBeta + " " + thetaBeta);
        double thetaAlpha = currTheta / currAlpha;
        if (Double.isNaN(thetaBeta)) thetaBeta = 0d;
        if (Double.isNaN(thetaAlpha)) thetaAlpha = 0d;
        ResultActivity.this.thetaBetaRatio = thetaBeta;
        ResultActivity.this.thetaAlphaRatio = thetaAlpha;


    }

    private void updateAccelerometer(final ArrayList<Double> data) {

    }

    private void updateEeg(final ArrayList<Double> data) {

    }

    private void updateAlphaRelative(final ArrayList<Double> data) {

        double temp = (data.get(Eeg.EEG2.ordinal())
                + data.get(Eeg.EEG2.ordinal())) / 2d;
        if (Double.isNaN(temp)) temp = 0d;
        else if (ResultActivity.this.currAlpha == 0d)
            ResultActivity.this.currAlpha = temp;
        else {
            ResultActivity.this.currAlpha = (ResultActivity.this.currAlpha + temp) / 2d;
        }
        Log.v("datatype", "alpha " + ResultActivity.this.currAlpha + " " + data);

    }

    private void updateBattery(final ArrayList<Double> data) {
        if (data.get(0) <= 20d) {
            if (!isBatteryBelow) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        isBatteryBelow = true;
                        AppModel.getInstance().popUpMessage(ResultActivity.this, "Battery below than 20% on your headset");
                    }
                });
            }
        }
    }

    private void animate(final HoloCircularProgressBar progressBar,
                         final Animator.AnimatorListener listener, double ratio) {
        final float progress = (Math.max(0f, (10f - (float) ratio))) / 10f;
//        final float progress = (Math.max(0f, (10f - (float) ResultActivity.this.thetaBetaRatio))) / 10f;
        if (progress > 0.6f && progressBar.getProgressColor() == progressColor) {
            progressBar.setProgressColor(Color.rgb(0, 255, 0));
        } else if (progress <= 0.6f && progressBar.getProgressColor() != progressColor) {
            progressBar.setProgressColor(progressColor);
        }
        int duration = 1000;
        animate(progressBar, listener, progress, duration);
    }

    private void animate(final HoloCircularProgressBar progressBar, final Animator.AnimatorListener listener,
                         final float progress, final int duration) {

        objectAnimator = ObjectAnimator.ofFloat(progressBar, "progress", progress);
        objectAnimator.setDuration(duration);

        objectAnimator.addListener(new Animator.AnimatorListener() {

            @Override
            public void onAnimationCancel(final Animator animation) {
            }

            @Override
            public void onAnimationEnd(final Animator animation) {
                progressBar.setProgress(progress);
            }

            @Override
            public void onAnimationRepeat(final Animator animation) {
            }

            @Override
            public void onAnimationStart(final Animator animation) {
            }
        });
        if (listener != null) {
            objectAnimator.addListener(listener);
        }
        objectAnimator.reverse();
        objectAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(final ValueAnimator animation) {
                progressBar.setProgress((Float) animation.getAnimatedValue());
            }
        });
        progressBar.setMarkerProgress(progress);
        objectAnimator.start();
    }

}
