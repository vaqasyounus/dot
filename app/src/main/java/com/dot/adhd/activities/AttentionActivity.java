package com.dot.adhd.activities;

import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.animation.RotateAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.choosemuse.libmuse.ConnectionState;
import com.choosemuse.libmuse.Eeg;
import com.choosemuse.libmuse.LibmuseVersion;
import com.choosemuse.libmuse.Muse;
import com.choosemuse.libmuse.MuseArtifactPacket;
import com.choosemuse.libmuse.MuseConnectionListener;
import com.choosemuse.libmuse.MuseConnectionPacket;
import com.choosemuse.libmuse.MuseDataListener;
import com.choosemuse.libmuse.MuseDataPacket;
import com.choosemuse.libmuse.MuseDataPacketType;
import com.choosemuse.libmuse.MuseListener;
import com.choosemuse.libmuse.MuseManagerAndroid;
import com.choosemuse.libmuse.MusePreset;
import com.dot.adhd.R;
import com.dot.adhd.models.AppModel;
import com.kyleduo.switchbutton.SwitchButton;
import com.theanilpaudel.rotatinganimation.Rotation;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import static com.choosemuse.libmuse.MuseDataPacketType.ALPHA_RELATIVE;
import static com.choosemuse.libmuse.MuseDataPacketType.ARTIFACTS;
import static com.choosemuse.libmuse.MuseDataPacketType.BATTERY;
import static com.choosemuse.libmuse.MuseDataPacketType.BETA_RELATIVE;
import static com.choosemuse.libmuse.MuseDataPacketType.DELTA_RELATIVE;
import static com.choosemuse.libmuse.MuseDataPacketType.EEG;
import static com.choosemuse.libmuse.MuseDataPacketType.THETA_RELATIVE;


public class AttentionActivity extends BottomBarActivity {

    private final String TAG = "TestLibMuseAndroid";
    private MuseManagerAndroid manager;
    private Muse muse;
    private ConnectionListener connectionListener;
    private DataListener dataListener;
    private final double[] eegBuffer = new double[6];
    private boolean eegStale;
    private final double[] alphaBuffer = new double[6];
    private boolean alphaStale;
    private final double[] accelBuffer = new double[3];
    private boolean accelStale;

    /**
     * We will be updating the UI using a handler instead of in packet handlers because
     * packets come in at a very high frequency and it only makes sense to update the UI
     * at about 60fps. The update functions do some string allocation, so this reduces our memory
     * footprint and makes GC pauses less frequent/noticeable.
     */
    private final Handler handler = new Handler();

    private boolean dataTransmission = true;

    Button startEnd;
    TextView timerText;
    TextView discription;
    long MillisecondTime, StartTime, TimeBuff, UpdateTime = 0L;
    Handler timeHandler;
    int Seconds, Minutes, MilliSeconds;
    ArrayAdapter<String> museListAdapter;
    Dialog connectDialog;
    volatile boolean[] isSensorOn = new boolean[4];
    TextView connect_tv;
    boolean isBatteryBelow;
    boolean isSensorActivate;
    double TBPercentage;
    double TAPercentage;
    boolean mAnimationHasEnded;
    AnimationDrawable animation;


    private double currAlpha = 0d;
    private double currBeta = 0d;
    private double currDelta = 0d;
    private double currTheta = 0d;
    private double thetaBetaRatio = 0d;
    private double thetaAlphaRatio = 0d;
    int sensorPosition;

    View sensor1;
    View sensor2;
    View sensor3;
    View sensor4;

    TextView proceed;


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.frame_main);
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View screen = layoutInflater.inflate(R.layout.activity_attention_n, null, false);
        frameLayout.addView(screen);
        AppModel.getInstance().currentActivity = AttentionActivity.this;

        attention.setBackgroundColor(ContextCompat.getColor(this, R.color.light_yellow));
        attention.setTextColor(ContextCompat.getColor(this, R.color.colorWhite));
        TextView tabTitle = (TextView) findViewById(R.id.tab_title);
        tabTitle.setText("Monitor");
        ImageView tabIcon = (ImageView) findViewById(R.id.tab_icon);
        tabIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AttentionActivity.this, DashboardActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
        timeHandler = new Handler();
        startEnd = (Button) findViewById(R.id.startEnd);
        animation = new AnimationDrawable();

        timerText = (TextView) findViewById(R.id.timerText);
        discription = (TextView) findViewById(R.id.discription);
        museListAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_single_choice);


        manager = MuseManagerAndroid.getInstance();
        manager.setContext(this);
        Log.i(TAG, "LibMuse version=" + LibmuseVersion.instance().getString());
        WeakReference<AttentionActivity> weakActivity =
                new WeakReference<AttentionActivity>(this);
        connectionListener = new ConnectionListener(weakActivity);
        dataListener = new DataListener(weakActivity);
        manager.setMuseListener(new MuseL(weakActivity));
        manager.startListening();
        startEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (startEnd.getText().equals("Start")) {
                    startEnd.setText("End");
                    StartTime = SystemClock.uptimeMillis();
                    handler.postDelayed(runnable, 0);

                    mAnimationHasEnded = false;
                    currAlpha = 0d;
                    currBeta = 0d;
                    currDelta = 0d;
                    currTheta = 0d;
                    thetaBetaRatio = 0d;
                    thetaAlphaRatio = 0d;
                    TBPercentage = 0d;
                    TAPercentage = 0d;
                    discription.setText("");

                } else {
                    mAnimationHasEnded = true;
                    startEnd.setText("Start");
                    MillisecondTime = 0L;
                    StartTime = 0L;
                    TimeBuff = 0L;
                    UpdateTime = 0L;
                    Seconds = 0;
                    Minutes = 0;
                    MilliSeconds = 0;

                    timerText.setText("00 : 00");
                    handler.removeCallbacks(runnable);
                    TBPercentage = ((10 - thetaBetaRatio) / 10d) * 100;
                    TAPercentage = ((5 - thetaAlphaRatio) / 5d) * 100;

                    startEnd.setText("Start");
                    if (((TBPercentage >= 0 && TBPercentage <= 100) && (TAPercentage >= 0 && TAPercentage <= 100))) {
//                                        concentrationPercentage.setText(String.format("%.2f", TBPercentage) + " %");
//                                        relaxationPercentage.setText(String.format("%.2f", TAPercentage) + " %");

                        //DatabaseHelper.getInstance(AttentionActivityN.this).addMonitorData(new MonitorDataModel(currTheta, currBeta, currAlpha, currDelta, thetaBetaRatio, thetaAlphaRatio, 0.0, System.currentTimeMillis(), 0));
                        String TBP = Double.toString(TBPercentage).substring(0, 2);
                        if (TBPercentage < 50) {
                            discription.setText(TBP + "% You really need to focus harder next time.");
                        } else if (TBPercentage >= 50 && TBPercentage < 70) {
                            discription.setText(TBP + "% Hmmn...you can do better than this.");
                        } else if (TBPercentage >= 70 && TBPercentage < 90) {
                            discription.setText(TBP + "% Great Job! You are on the right track.");
                        } else if (TBPercentage >= 90) {
                            discription.setText(TBP + "% Your concentration was fantastic.");
                        }

                    } else {
                        discription.setText("Whoops! something went wrong please try again.");
                        finish();
                    }

                }
            }
        });
        showDialog();

    }

    @Override
    protected void onPause() {
        super.onPause();
        manager.stopListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (muse != null)
            muse.disconnect();
    }

    public void museListChanged() {
        final List<Muse> list = manager.getMuses();
        museListAdapter.clear();
        for (Muse m : list) {
            museListAdapter.add(m.getName() + " - " + m.getMacAddress());
        }
    }

    public void receiveMuseConnectionPacket(final MuseConnectionPacket p, final Muse muse) {

        final ConnectionState current = p.getCurrentConnectionState();

        // Format a message to show the change of connection state in the UI.
        final String status = p.getPreviousConnectionState() + " -> " + current;
        Log.i(TAG, status);

        // Update the UI with the change in connection state.
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (current == ConnectionState.CONNECTED) {
                    connect_tv.setText(current.name());
                    AppModel.getInstance().popUpMessage(AttentionActivity.this, "Waiting for sensors to get activated");
                } else if (current == ConnectionState.DISCONNECTED) {
                    AppModel.getInstance().popUpMessage(AttentionActivity.this, "Headset not found");
                    connect_tv.setText(current.name());
                    /*if (!connectDialog.isShowing()) {
                        connectDialog.show();
                    }*/
                } else {
                    connect_tv.setText(current.name());
                    AppModel.getInstance().popUpMessage(AttentionActivity.this, "connecting to your headset");
                }
            }
        });

        if (current == ConnectionState.DISCONNECTED) {
            Log.i(TAG, "Muse disconnected:" + muse.getName());
            // We have disconnected from the headband, so set our cached copy to null.
            this.muse = null;
        }
    }

    public void receiveMuseDataPacket(final MuseDataPacket p, final Muse muse) {
        // valuesSize returns the number of data values contained in the packet.
        handler.post(new Runnable() {
            @Override
            public void run() {
                try {
                    ArrayList<Double> data = new ArrayList<>();
                    data.add(p.values().get(1));
                    data.add(p.values().get(2));
                    data.add(p.values().get(3));
                    data.add(p.values().get(4));

                    if (p.packetType() == ALPHA_RELATIVE || p.packetType() == EEG || p.packetType() == BETA_RELATIVE || p.packetType() == THETA_RELATIVE || p.packetType() == DELTA_RELATIVE) {
                        for (int i = 0; i < data.size(); i++)
                            isSensorOn[i] = !data.get(i).isNaN();
                        int count = 0;
                        for (sensorPosition = 0; sensorPosition < isSensorOn.length; sensorPosition++) {


                            if (isSensorOn[sensorPosition]) {
                                count++;

                                switch (sensorPosition) {
                                    case 0:
                                        sensor1.setBackgroundColor(Color.GREEN);
                                        break;
                                    case 1:
                                        sensor2.setBackgroundColor(Color.GREEN);
                                        break;
                                    case 2:
                                        sensor3.setBackgroundColor(Color.GREEN);
                                        break;
                                    case 3:
                                        sensor4.setBackgroundColor(Color.GREEN);
                                        break;

                                }

                            } else {
                                switch (sensorPosition) {
                                    case 0:
                                        sensor1.setBackgroundColor(Color.GRAY);
                                        break;
                                    case 1:
                                        sensor2.setBackgroundColor(Color.GRAY);
                                        break;
                                    case 2:
                                        sensor3.setBackgroundColor(Color.GRAY);
                                        break;
                                    case 3:
                                        sensor4.setBackgroundColor(Color.GRAY);
                                        break;

                                }

                            }
                        }
                        if (count >= 2) {

                            if (!isSensorActivate) {
                                isSensorActivate = true;
                                if (connectDialog.isShowing()) {
                                    proceed.setTextColor(Color.GREEN);
                                    AppModel.getInstance().popUpMessage(AttentionActivity.this, "Sensors activated tap to proceed");
                                } else {
                                    AppModel.getInstance().popUpMessage(AttentionActivity.this, "Sensors activated again");
                                }
                            }

                        } else {

                            if (isSensorActivate) {
                                isSensorActivate = false;
                                if (!connectDialog.isShowing()) {
                                    connectDialog.show();
                                    AppModel.getInstance().popUpMessage(AttentionActivity.this, "Sensors deactivated adjust your device");
                                    proceed.setTextColor(ContextCompat.getColor(AttentionActivity.this, R.color.colorBlackTransparent));
                                } else {
                                    AppModel.getInstance().popUpMessage(AttentionActivity.this, "Sensors deactivated adjust your device");
                                    proceed.setTextColor(ContextCompat.getColor(AttentionActivity.this, R.color.colorBlackTransparent));

                                }
                            }

                        }
                    }
                } catch (Exception e) {
                    Log.d("Exception", "");
                }
                switch (p.packetType()) {
                    case EEG:
                        updateEeg(p.values());
                        break;
                    case ALPHA_RELATIVE:
                        updateAlphaRelative(p.values());
                        break;
                    case BETA_RELATIVE:
                        updateBetaRelative(p.values());
                        break;
                    case THETA_RELATIVE:
                        updateThetaRelative(p.values());
                        break;
                    case DELTA_RELATIVE:
                        updateDeltaRelative(p.values());
                        break;
                    case ACCELEROMETER:
                        updateAccelerometer(p.values());
                        break;
                    case BATTERY:
                        updateBattery(p.values());
                        break;
                    default:
                        break;
                }
            }
        });
    }

    public void receiveMuseArtifactPacket(final MuseArtifactPacket p, final Muse muse) {

        handler.post(new Runnable() {
            @Override
            public void run() {
               /* if (!p.getHeadbandOn()) {
                    connect_tv.setText("Put headband on your forhead");
                }else{
                    connect_tv.setText("Connected");
                }*/
            }
        });

    }

    class MuseL extends MuseListener {
        final WeakReference<AttentionActivity> activityRef;

        MuseL(final WeakReference<AttentionActivity> activityRef) {
            this.activityRef = activityRef;
        }

        @Override
        public void museListChanged() {
            activityRef.get().museListChanged();
        }
    }

    class ConnectionListener extends MuseConnectionListener {
        final WeakReference<AttentionActivity> activityRef;

        ConnectionListener(final WeakReference<AttentionActivity> activityRef) {
            this.activityRef = activityRef;
        }

        @Override
        public void receiveMuseConnectionPacket(final MuseConnectionPacket p, final Muse muse) {
            activityRef.get().receiveMuseConnectionPacket(p, muse);
        }
    }

    class DataListener extends MuseDataListener {
        final WeakReference<AttentionActivity> activityRef;

        DataListener(final WeakReference<AttentionActivity> activityRef) {
            this.activityRef = activityRef;
        }

        @Override
        public void receiveMuseDataPacket(final MuseDataPacket p, final Muse muse) {
            activityRef.get().receiveMuseDataPacket(p, muse);
        }

        @Override
        public void receiveMuseArtifactPacket(final MuseArtifactPacket p, final Muse muse) {
            activityRef.get().receiveMuseArtifactPacket(p, muse);
        }
    }

    public void showDialog() {
        final BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        connectDialog = new Dialog(this);
        connectDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        connectDialog.setContentView(R.layout.dialog_layout);
        SwitchButton status = (SwitchButton) connectDialog.findViewById(R.id.status);
        TextView scan = (TextView) connectDialog.findViewById(R.id.scan);
        final ListView musesList = (ListView) connectDialog.findViewById(R.id.muses_list);
        final TextView connect = (TextView) connectDialog.findViewById(R.id.connect);
        proceed = (TextView) connectDialog.findViewById(R.id.proceed);
        TextView cancel = (TextView) connectDialog.findViewById(R.id.cancel);
        status.setChecked(bluetoothAdapter.isEnabled());
        status.setClickable(false);
        proceed.setTextColor(ContextCompat.getColor(AttentionActivity.this, R.color.colorBlackTransparent));
        connect_tv = (TextView) connectDialog.findViewById(R.id.status_tv);
        sensor1 = connectDialog.findViewById(R.id.sensor1);
        sensor2 = connectDialog.findViewById(R.id.sensor2);
        sensor3 = connectDialog.findViewById(R.id.sensor3);
        sensor4 = connectDialog.findViewById(R.id.sensor4);

        musesList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        musesList.setAdapter(museListAdapter);

        SparseBooleanArray checkedItemPositions = musesList.getCheckedItemPositions();
        if (checkedItemPositions.size() > 0) {
            connect.setTextColor(ContextCompat.getColor(AttentionActivity.this, R.color.colorDarkBlue1));
        } else {
            connect.setTextColor(ContextCompat.getColor(AttentionActivity.this, R.color.colorBlackTransparent));
        }
        musesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long id) {
                musesList.setItemChecked(i, true);
                connect.setTextColor(ContextCompat.getColor(AttentionActivity.this, R.color.colorDarkBlue1));
                connect.setText("Connect");
                muse = manager.getMuses().get(i);
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (muse != null)
                    muse.disconnect();
                finish();
            }
        });
        scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manager.stopListening();
                manager.startListening();
            }
        });
        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = 0;
                for (int i = 0; i < isSensorOn.length; i++) {
                    if (isSensorOn[i]) {
                        count++;
                    }
                }
                if (count >= 2) {
                    connectDialog.dismiss();
                } else {
                    AppModel.getInstance().popUpMessage(AttentionActivity.this, "Sensors are still inactive");
                }


            }
        });
        connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!bluetoothAdapter.isEnabled()) {
                    AppModel.getInstance().popUpMessage(AttentionActivity.this, "Turn ON your bluetooth");
                    return;
                }
//                muse.unregisterAllListeners();
                muse.registerConnectionListener(connectionListener);
                muse.registerDataListener(dataListener, ALPHA_RELATIVE);
                muse.registerDataListener(dataListener, BETA_RELATIVE);
                muse.registerDataListener(dataListener, THETA_RELATIVE);
                muse.registerDataListener(dataListener, DELTA_RELATIVE);
                muse.registerDataListener(dataListener, BATTERY);
//                muse.registerDataListener(dataListener, ARTIFACTS);
                muse.setPreset(MusePreset.PRESET_14);

                muse.runAsynchronously();

            }
        });
        connectDialog.setCancelable(false);

        connectDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        connectDialog.show();
    }

    public Runnable runnable = new Runnable() {

        public void run() {

            MillisecondTime = SystemClock.uptimeMillis() - StartTime;

            UpdateTime = TimeBuff + MillisecondTime;

            Seconds = (int) (UpdateTime / 1000);

            Minutes = Seconds / 60;

            Seconds = Seconds % 60;

            MilliSeconds = (int) (UpdateTime % 1000);
            String stringMinutes = "" + Minutes;
            if (stringMinutes.length() == 1) {
                stringMinutes = "0" + stringMinutes;
            }
            timerText.setText("" + stringMinutes + " : "
                    + String.format("%02d", Seconds)); /*":"
                    + String.format("%03d", MilliSeconds));*/

            handler.postDelayed(this, 0);
        }

    };

    void updateDeltaRelative(final ArrayList<Double> data) {
        Log.v("datatype", "delta " + AttentionActivity.this.currDelta + " " + data);
        double temp = (data.get(Eeg.EEG2.ordinal()) + data.get(Eeg.EEG2.ordinal())) / 2d;
        if (Double.isNaN(temp)) return;
        else if (AttentionActivity.this.currDelta == 0d)
            AttentionActivity.this.currDelta = temp;
        else {
            AttentionActivity.this.currDelta = (AttentionActivity.this.currDelta + temp) / 2d;
        }

    }

    void updateBetaRelative(final ArrayList<Double> data) {


        Log.v("datatype", "beta " + AttentionActivity.this.currBeta + " " + data);
        double temp = (data.get(Eeg.EEG2.ordinal())
                + data.get(Eeg.EEG2.ordinal())) / 2d;
        if (Double.isNaN(temp)) return;
        else if (AttentionActivity.this.currBeta == 0d)
            AttentionActivity.this.currBeta = temp;
        else {
            AttentionActivity.this.currBeta = (AttentionActivity.this.currBeta + temp) / 2d;
            double progress = ((10 - thetaBetaRatio) / 10d) * 100;

            Log.d("showprog", "" + progress);


            if (progress > 50 && progress <= 65) {
                showDialogNotify();
            }
        }
    }

    void updateThetaRelative(final ArrayList<Double> data) {
        //we're only interested in getting data from the 2 forehead sensors
        Log.v("datatype", "theta " + AttentionActivity.this.currTheta + " " + data);
        double temp = (data.get(Eeg.EEG2.ordinal())
                + data.get(Eeg.EEG2.ordinal())) / 2d;
        if (Double.isNaN(temp)) return;
        else if (AttentionActivity.this.currTheta == 0d)
            AttentionActivity.this.currTheta = temp;
        else {
            AttentionActivity.this.currTheta = (AttentionActivity.this.currTheta + temp) / 2d;
        }

        double thetaBeta = currTheta / currBeta;
        Log.v("ratios", currTheta + " " + currBeta + " " + thetaBeta);
        double thetaAlpha = currTheta / currAlpha;
        if (Double.isNaN(thetaBeta)) thetaBeta = 0d;
        if (Double.isNaN(thetaAlpha)) thetaAlpha = 0d;
        AttentionActivity.this.thetaBetaRatio = thetaBeta;
        AttentionActivity.this.thetaAlphaRatio = thetaAlpha;


    }

    private void updateAccelerometer(final ArrayList<Double> data) {

    }

    private void updateEeg(final ArrayList<Double> data) {

    }

    private void updateAlphaRelative(final ArrayList<Double> data) {

        double temp = (data.get(Eeg.EEG2.ordinal())
                + data.get(Eeg.EEG2.ordinal())) / 2d;
        if (Double.isNaN(temp)) temp = 0d;
        else if (AttentionActivity.this.currAlpha == 0d)
            AttentionActivity.this.currAlpha = temp;
        else {
            AttentionActivity.this.currAlpha = (AttentionActivity.this.currAlpha + temp) / 2d;
        }
        Log.v("datatype", "alpha " + AttentionActivity.this.currAlpha + " " + data);

    }

    private void updateBattery(final ArrayList<Double> data) {
        if (data.get(0) <= 20d) {
            if (!isBatteryBelow) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        isBatteryBelow = true;
                        AppModel.getInstance().popUpMessage(AttentionActivity.this, "Battery below than 20% on your headset");
                    }
                });
            }
        }
    }

    public void showDialogNotify() {
        Dialog dialogNotify;
        dialogNotify = new Dialog(this);
        dialogNotify.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogNotify.setContentView(R.layout.dialog_layout_notify);

        TextView notifyText = (TextView) dialogNotify.findViewById(R.id.notifytext);
        notifyText.setText("You are loosing focus. \nPay attention!");


        dialogNotify.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        if (!dialogNotify.isShowing())
//            dialogNotify.show();
    }


}
