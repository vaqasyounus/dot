package com.dot.adhd.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.dot.adhd.R;
import com.dot.adhd.models.AppModel;

public class BottomBarActivity extends AppCompatActivity implements View.OnClickListener {

    protected TextView monitor, games, scores, progress,attention;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottom_bar);
        Toolbar toolbar = (Toolbar) findViewById(R.id.id_toolbar);
        setSupportActionBar(toolbar);

        monitor = (TextView) findViewById(R.id.monitor);
        games = (TextView) findViewById(R.id.games);
        scores = (TextView) findViewById(R.id.scores);
        progress = (TextView) findViewById(R.id.progress);
        attention = (TextView) findViewById(R.id.attension);

        monitor.setOnClickListener(this);
        games.setOnClickListener(this);
        scores.setOnClickListener(this);
        progress.setOnClickListener(this);
        attention.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        monitor.setBackground(null);
        games.setBackground(null);
        scores.setBackground(null);
        progress.setBackground(null);
        attention.setBackground(null);

        monitor.setTextColor(Color.WHITE);
        games.setTextColor(Color.WHITE);
        scores.setTextColor(Color.WHITE);
        progress.setTextColor(Color.WHITE);
        attention.setTextColor(Color.WHITE);


        switch (v.getId()) {
            case R.id.monitor:
                monitor.setBackgroundColor(ContextCompat.getColor(this, R.color.light_yellow));
                monitor.setTextColor(ContextCompat.getColor(this, R.color.colorWhite));
                if (!(AppModel.getInstance().currentActivity instanceof ResultActivity)) {
                    startActivity(new Intent(BottomBarActivity.this, ResultActivity.class));
                    finish();
                }
                break;
            case R.id.games:
                games.setBackgroundColor(ContextCompat.getColor(this, R.color.light_yellow));
                games.setTextColor(ContextCompat.getColor(this, R.color.colorWhite));
                if (!(AppModel.getInstance().currentActivity instanceof GamesActivity)) {
                    startActivity(new Intent(BottomBarActivity.this, GamesActivity.class));
                    finish();
                }
                break;
            case R.id.scores:
                scores.setBackgroundColor(ContextCompat.getColor(this, R.color.light_yellow));
                scores.setTextColor(ContextCompat.getColor(this, R.color.colorWhite));
                if (!(AppModel.getInstance().currentActivity instanceof ScoresActivity)) {
                    startActivity(new Intent(BottomBarActivity.this, ScoresActivity.class));
                    finish();
                }
                break;
            case R.id.progress:
                progress.setBackgroundColor(ContextCompat.getColor(this, R.color.light_yellow));
                progress.setTextColor(ContextCompat.getColor(this, R.color.colorWhite));
                if (!(AppModel.getInstance().currentActivity instanceof ProgressActivity)) {
                    startActivity(new Intent(BottomBarActivity.this, ProgressActivity.class));
                    finish();
                }
                break;
            case R.id.attension:
                progress.setBackgroundColor(ContextCompat.getColor(this, R.color.light_yellow));
                progress.setTextColor(ContextCompat.getColor(this, R.color.colorWhite));
                if (!(AppModel.getInstance().currentActivity instanceof AttentionActivity)) {
                    startActivity(new Intent(BottomBarActivity.this, AttentionActivity.class));
                    finish();
                }
                break;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
