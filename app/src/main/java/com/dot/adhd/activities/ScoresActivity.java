package com.dot.adhd.activities;

import android.content.Intent;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.dot.adhd.R;
import com.dot.adhd.models.AppModel;

import java.util.ArrayList;
import java.util.List;

import belka.us.androidtoggleswitch.widgets.BaseToggleSwitch;
import belka.us.androidtoggleswitch.widgets.ToggleSwitch;
import lecho.lib.hellocharts.gesture.ContainerScrollType;
import lecho.lib.hellocharts.gesture.ZoomType;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.view.LineChartView;

public class ScoresActivity extends BottomBarActivity {
    LineChartView chart;
    ToggleSwitch toggleSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.frame_main);
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View screen = layoutInflater.inflate(R.layout.activity_scores, null, false);
        frameLayout.addView(screen);
        AppModel.getInstance().currentActivity = ScoresActivity.this;

        scores.setBackgroundColor(ContextCompat.getColor(this, R.color.light_yellow));
        scores.setTextColor(ContextCompat.getColor(this, R.color.colorWhite));

        TextView tabTitle = (TextView) findViewById(R.id.tab_title);
        tabTitle.setText("Scores");
        ImageView tabIcon = (ImageView) findViewById(R.id.tab_icon);
        tabIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ScoresActivity.this, DashboardActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        chart = (LineChartView) findViewById(R.id.chart_theta_alpha);
        toggleSwitch = (ToggleSwitch) findViewById(R.id.switches);


        final List<PointValue> values = new ArrayList<PointValue>();
        AppModel.getInstance().drawGraph(ScoresActivity.this, values, chart);
        toggleSwitch.setToggleWidth((float) 140);
        ArrayList<String> labels = new ArrayList<>();
        labels.add("Day");
        labels.add("Week");
        labels.add("Month");
        labels.add("4 Months");
        labels.add("8 Months");
        labels.add("Year");

        toggleSwitch.setLabels(labels);
        toggleSwitch.setOnToggleSwitchChangeListener(new BaseToggleSwitch.OnToggleSwitchChangeListener() {
            @Override
            public void onToggleSwitchChangeListener(int position, boolean isChecked) {
                switch (position) {
                    case 0:
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                }
            }
        });


    }
}
