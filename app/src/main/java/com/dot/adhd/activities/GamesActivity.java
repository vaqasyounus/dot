package com.dot.adhd.activities;

import android.content.Intent;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.dot.adhd.R;
import com.dot.adhd.models.AppModel;

public class GamesActivity extends BottomBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.frame_main);
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View screen = layoutInflater.inflate(R.layout.activity_games, null, false);
        frameLayout.addView(screen);
        AppModel.getInstance().currentActivity = GamesActivity.this;

        games.setBackgroundColor(ContextCompat.getColor(this, R.color.light_yellow));
        games.setTextColor(ContextCompat.getColor(this, R.color.colorWhite));

        TextView tabTitle = (TextView) findViewById(R.id.tab_title);
        tabTitle.setText("Games");
        ImageView tabIcon = (ImageView) findViewById(R.id.tab_icon);
        tabIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GamesActivity.this, DashboardActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
    }

    public void goToAnim(View view) {
        Intent intent = new Intent(GamesActivity.this, GameAnimActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    public void gotoScore(View view) {
        Intent intent = new Intent(GamesActivity.this, ScoresActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
