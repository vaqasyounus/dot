package com.dot.adhd.activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.annotation.IdRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.dot.adhd.R;
import com.dot.adhd.models.AppModel;
import com.dot.adhd.models.UserModel;
import com.dot.adhd.retrofit.ApiClient;
import com.dot.adhd.retrofit.ApiInterface;

import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText parent_name;
    private EditText email;
    private EditText password;
    private EditText child_name;
    private EditText child_age;
    private RadioGroup child_gender;
    private String child_gender_txt;
    private RadioButton male;
    private RadioButton female;
    private RadioButton other;
    private EditText child_diagnosis_date;
    private EditText child_medication;
    private Button signUp;
    private RelativeLayout parentLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        parent_name = (EditText) findViewById(R.id.parents_name);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        child_name = (EditText) findViewById(R.id.child_name);
        child_age = (EditText) findViewById(R.id.age);
        child_gender = (RadioGroup) findViewById(R.id.gender);

        male = (RadioButton) findViewById(R.id.male);
        female = (RadioButton) findViewById(R.id.female);
        other = (RadioButton) findViewById(R.id.other);

        child_diagnosis_date = (EditText) findViewById(R.id.diagnosis_date);
        child_medication = (EditText) findViewById(R.id.medication);
        signUp = (Button) findViewById(R.id.signUp);
        parentLayout = (RelativeLayout) findViewById(R.id.parentLayout);

        password.setTransformationMethod(new PasswordTransformationMethod());
        signUp.setOnClickListener(this);
        child_gender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                male.setTextColor(ContextCompat.getColor(RegisterActivity.this, R.color.colorLightGray));
                female.setTextColor(ContextCompat.getColor(RegisterActivity.this, R.color.colorLightGray));
                other.setTextColor(ContextCompat.getColor(RegisterActivity.this, R.color.colorLightGray));
                switch (checkedId) {
                    case R.id.male:
                        male.setTextColor(ContextCompat.getColor(RegisterActivity.this, R.color.colorGray));
                        child_gender_txt = "male";
                        break;
                    case R.id.female:
                        child_gender_txt = "female";
                        female.setTextColor(ContextCompat.getColor(RegisterActivity.this, R.color.colorGray));
                        break;
                    case R.id.other:
                        child_gender_txt = "other";
                        other.setTextColor(ContextCompat.getColor(RegisterActivity.this, R.color.colorGray));
                        break;
                }
            }
        });

    }

    public void signIn(View v) {
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.signUp:
                if (AppModel.getInstance().isValid(parentLayout)) {
                    final AlertDialog dialog = new SpotsDialog(RegisterActivity.this);
                    dialog.show();
                    Map<String, String> data = new HashMap<>();
                    data.put("name", parent_name.getText().toString());
                    data.put("email", email.getText().toString());
                    data.put("password", password.getText().toString());
                    data.put("child_name", child_name.getText().toString());
                    data.put("child_age", child_age.getText().toString());
                    data.put("child_gender", child_gender_txt);
                    data.put("child_diagnosis_date", child_diagnosis_date.getText().toString().trim());
                    data.put("child_medication", child_medication.getText().toString().trim());
                    ApiClient.getClient().create(ApiInterface.class).registerAPI(data).enqueue(new Callback<UserModel>() {
                        @Override
                        public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                            dialog.dismiss();
                            if (response.isSuccessful() && response.body().isError()==false) {
                                    AppModel.getInstance().popUpMessage(RegisterActivity.this, response.body().getMessage());
                                    startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                                    finish();

                            } else {
                                AppModel.getInstance().popUpMessage(RegisterActivity.this, response.body().getMessage());
                            }
                        }

                        @Override
                        public void onFailure(Call<UserModel> call, Throwable t) {
                            dialog.dismiss();
                            AppModel.getInstance().popUpMessage(RegisterActivity.this, "Network failure");

                        }
                    });

                }
        }
    }
}
