package com.dot.adhd.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.dot.adhd.R;
import com.dot.adhd.models.AppConstant;
import com.dot.adhd.models.AppModel;

import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends AppCompatActivity {
    long delayTime = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {

                String isLogin = AppModel.getInstance().readFromSharedPrefrence(getApplicationContext(), AppConstant.API_KEY_SP);
                if (isLogin.equals("false")) {
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
//                    startActivity(new Intent(SplashActivity.this, AttentionActivitywithnewLib.class));
                } else {
                    startActivity(new Intent(SplashActivity.this, DashboardActivity.class));
//                    startActivity(new Intent(SplashActivity.this, AttentionActivitywithnewLib.class));
                }
                finish();
            }
        };
        Timer timer = new Timer();
        timer.schedule(timerTask, delayTime);
    }

    @Override
    public void onBackPressed() {

    }
}
