package com.dot.adhd.models;

/**
 * Created by Muhammad Waqas on 5/2/2017.
 */

public class MonitorDataModel {
    int id;
    double theta;
    double beta;
    double alpha;
    double delta;
    double theta_beta_ratio;
    double theta_alpha_ratio;
    double eeg;
    long createdAt;
    int syncStatus;

    public MonitorDataModel() {
    }

    public MonitorDataModel(double theta, double beta, double alpha, double delta, double thetaBetaRaio, double thetaAlphaRatio, double eeg, long timeSamp, int syncStatus) {

        this.theta = theta;
        this.beta = beta;
        this.alpha = alpha;
        this.delta = delta;
        this.theta_beta_ratio = thetaBetaRaio;
        this.theta_alpha_ratio = thetaAlphaRatio;
        this.eeg = eeg;
        this.createdAt = timeSamp;
        this.syncStatus = syncStatus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getTheta() {
        return theta;
    }

    public void setTheta(double theta) {
        this.theta = theta;
    }

    public double getBeta() {
        return beta;
    }

    public void setBeta(double beta) {
        this.beta = beta;
    }

    public double getAlpha() {
        return alpha;
    }

    public void setAlpha(double alpha) {
        this.alpha = alpha;
    }

    public double getDelta() {
        return delta;
    }

    public void setDelta(double delta) {
        this.delta = delta;
    }

    public double getTheta_beta_ratio() {
        return theta_beta_ratio;
    }

    public void setTheta_beta_ratio(double theta_beta_ratio) {
        this.theta_beta_ratio = theta_beta_ratio;
    }

    public double getTheta_alpha_ratio() {
        return theta_alpha_ratio;
    }

    public void setTheta_alpha_ratio(double theta_alpha_ratio) {
        this.theta_alpha_ratio = theta_alpha_ratio;
    }

    public double getEeg() {
        return eeg;
    }

    public void setEeg(double eeg) {
        this.eeg = eeg;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public int getSyncStatus() {
        return syncStatus;
    }

    public void setSyncStatus(int syncStatus) {
        this.syncStatus = syncStatus;
    }
}
