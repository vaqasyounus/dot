package com.dot.adhd.models;

import java.util.ArrayList;

/**
 * Created by Muhammad Waqas on 5/10/2017.
 */

public class PointModel {
    boolean error;
    String message;
    int pointId;

    public int getPointId() {
        return pointId;
    }

    public void setPointId(int pointId) {
        this.pointId = pointId;
    }

    ArrayList<MonitorDataModel> points;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public ArrayList<MonitorDataModel> getPoints() {
        return points;
    }

    public void setPoints(ArrayList<MonitorDataModel> points) {
        this.points = points;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
