package com.dot.adhd.models;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.content.ContextCompat;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dot.adhd.R;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import lecho.lib.hellocharts.gesture.ZoomType;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.view.LineChartView;

/**
 * Created by Muhammad Waqas on 4/20/2017.
 */

public class AppModel {
    public Context currentActivity;
    private static final AppModel ourInstance = new AppModel();

    public static AppModel getInstance() {
        return ourInstance;
    }

    private AppModel() {
    }

    public boolean isConnnected(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (!(networkInfo != null && networkInfo.isConnected())) {

            AppModel.getInstance().popUpMessage(context, "Network failure");
        }
        return (networkInfo != null && networkInfo.isConnected());
    }

    public void writeToSharedPrefrence(Context context, String key, String value) {
        SharedPreferences sharedPref = context.getSharedPreferences(AppConstant.DOT_SHARED_PREF, Context.MODE_WORLD_WRITEABLE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String readFromSharedPrefrence(Context context, String key) {
        SharedPreferences sharedPref = context.getSharedPreferences(AppConstant.DOT_SHARED_PREF, Context.MODE_WORLD_READABLE);
        return sharedPref.getString(key, "false");
    }

    public void removeSharedPrefrence(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(AppConstant.DOT_SHARED_PREF, Context.MODE_WORLD_READABLE);
        sharedPref.edit().clear().commit();

    }

    public void hideKeyboard(ViewGroup viewGroup, Context context) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(viewGroup.getWindowToken(), 0);
    }

    public boolean isValid(ViewGroup viewGroup) {
        for (View view : viewGroup.getTouchables()) {
            if (view instanceof EditText) {
                EditText editText = (EditText) view;
                String text = editText.getText().toString().trim();
                if (text.isEmpty() && !(editText.getHint().toString().equals("Child's Diagnosis Date") || editText.getHint().toString().equals("Child's Medication"))) {
                    editText.setError("Required");
                    editText.requestFocus();
                    return false;
                } else {
                    if (editText.getInputType() == InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS + InputType.TYPE_CLASS_TEXT) {
                        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(text).matches()) {
                            editText.setError("Invalid Email");
                            editText.requestFocus();
                            return false;
                        } else {
                            editText.setError(null);
                        }
                    } else {
                        editText.setError(null);
                    }
                }
            }
        }

        return true;
    }

    public void popUpMessage(Context context, String message) {
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
        View view = toast.getView();
        view.setBackgroundResource(R.drawable.round_corner_red);
        TextView text = (TextView) view.findViewById(android.R.id.message);
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/to-the-point.regular.ttf");
        toast.setGravity(Gravity.CENTER, 0, 0);
        text.setTypeface(typeface);
        text.setTextColor(ContextCompat.getColor(context, R.color.colorWhite));
        String fontSize = context.getString(R.string.toast_font_size);
        text.setTextSize(Integer.parseInt(fontSize));
        toast.show();
    }

    public void popUpMessage(Context context, String message, int duration) {
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
        View view = toast.getView();
        view.setBackgroundResource(R.drawable.round_corner_red);
        TextView text = (TextView) view.findViewById(android.R.id.message);
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/to-the-point.regular.ttf");
        toast.setGravity(Gravity.CENTER, 0, 0);
        text.setTypeface(typeface);
        text.setTextColor(ContextCompat.getColor(context, R.color.colorWhite));
        String fontSize = context.getString(R.string.toast_font_size);
        text.setTextSize(Integer.parseInt(fontSize));
        toast.show();
    }

    public void drawGraph(Context context, List<PointValue> values, LineChartView lineChartView) {
        lineChartView.setInteractive(true);
        lineChartView.setZoomType(ZoomType.HORIZONTAL);
        int pointRadius;
        if (values.size() <= 20) {
            pointRadius = 6;
        } else if (values.size() > 20 && values.size() <= 50) {
            pointRadius = 4;
        } else if (values.size() > 50 && values.size() <= 100) {
            pointRadius = 3;
        } else {
            pointRadius = 2;
        }
        Line line = new Line(values).setColor(ContextCompat.getColor(context, R.color.colorPrimary))
                .setCubic(true)
                .setPointColor(ContextCompat.getColor(context, R.color.colorAccent)).setPointRadius(pointRadius);
        List<Line> lines = new ArrayList<>();
        lines.add(line);

        LineChartData data = new LineChartData();

        data.setLines(lines);

        lineChartView.setLineChartData(data);
    }

    public Date timeStampToDate(long timeStamp) {
        return new Date(timeStamp);
    }

    public Date currentDate() {
        return new Date(System.currentTimeMillis());
    }

    public Date previous_n_days(int n) {
        return new Date(System.currentTimeMillis() - n * 24 * 3600 * 1000l); //Subtract n days
    }

    public long previous_n_days_milli_scond(int n) {
        long ms = (System.currentTimeMillis() - n * 24 * 3600 * 1000l);
        return ms;
    }

}
