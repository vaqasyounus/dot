package com.dot.adhd.models;

/**
 * Created by Muhammad Waqas on 2/23/2017.
 */

public class AppConstant {
    public static final String CONTENT_AUTHORITY = "com.dot.adhd.syncservice.datasync";
    public static final String CONTENT_PROVIDER_URI = "com.dot.adhd.syncservice.datasync.infoprovider";

    public static final String DOT_SHARED_PREF = "dotsharedpref";
    public static final String IS_GET_DATA_SP = "isgetdata";
    public static final String API_KEY_SP = "apikey";
    public static final String KEY_SP_Name = "keyname";

    public static final String URL_REGISTER = "api/register";
    public static final String URL_LOGIN = "api/login";
    public static final String URL_POINTS = "api/points";




    public static final int NOTIFICATION_ID = 1;
    public static final int FOREGROUND_SERVICE_ID = 1;

}
